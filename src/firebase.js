import firebase from "firebase/app";
import "firebase/firestore"; //dbs firestore

var firebaseConfig = {
  apiKey: "AIzaSyC88IO5XJAdvLCfAqSn8VuEfXmjkspd3PU",
  authDomain: "svelte-fire-9b32c.firebaseapp.com",
  databaseURL: "https://svelte-fire-9b32c.firebaseio.com",
  projectId: "svelte-fire-9b32c",
  storageBucket: "svelte-fire-9b32c.appspot.com",
  messagingSenderId: "643945845882",
  appId: "1:643945845882:web:2533e99e6239750e8057c3",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();
